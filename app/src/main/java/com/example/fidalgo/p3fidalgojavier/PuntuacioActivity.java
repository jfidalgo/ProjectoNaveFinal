package com.example.fidalgo.p3fidalgojavier;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PuntuacioActivity extends AppCompatActivity implements View.OnClickListener {

    String nomPersona;
    boolean encert;
    String operacio;
    private SQLiteDatabase db;

    TextView viewRegistres;

    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puntuacio);

        viewRegistres = (TextView) findViewById(R.id.textView3);

        Button buttonP = (Button) findViewById(R.id.buttonPunt);
        buttonP.setOnClickListener(this);

        Button buttonInici = (Button) findViewById(R.id.TornaInici);
        buttonInici.setOnClickListener(this);

        //Intent a la ultima activity
        i = new Intent(this, MainActivity.class);

        Bundle extras = getIntent().getExtras();
        //Nom de la persona
        nomPersona = extras.getString("nomPersona");
        //Boolean si ha encertat totes les preguntes
        encert = extras.getBoolean("encertat");
        //Operacio Feta
        operacio = extras.getString("operacio");


        TextView missatge = (TextView) findViewById(R.id.textViewPuntuacio);


        if (encert == true) {
            missatge.setText(nomPersona + ", has respos be el questionari de la operacio " + operacio);
        } else {
            missatge.setText(nomPersona + ", has suspes el questionari de la operacio " + operacio);
        }


        //Abrimos la base de datos 'DBUsuarios' en modo escritura
        UsuariosSQLiteHelper usdbh =
                new UsuariosSQLiteHelper(this, "DBUsuarios", null, 1);

        db = usdbh.getWritableDatabase();

        //Inserim a la base de dades l'usuari que ha fet el text, el tema i si ha aprobat
        ContentValues nuevoRegistro = new ContentValues();
        nuevoRegistro.put("nombre", nomPersona);
        nuevoRegistro.put("operacio", operacio);
        nuevoRegistro.put("superat", encert);
        db.insert("Usuarios", null, nuevoRegistro);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonPunt) {

            //Boto per consultar tots els resultats de les probes del mateix usuari
            Cursor c = db.rawQuery("SELECT * FROM Usuarios WHERE nombre='" + nomPersona + "'", null);

            viewRegistres.setText("");

            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya mбs registros
                do {
                    String nom = c.getString(0);
                    String operacio = c.getString(1);
                    String superat = c.getString(2);

                    String aprovat;
                    if (superat.equalsIgnoreCase("1")) {
                        aprovat = "NivellAprovat";
                    } else {
                        aprovat = "nivellSuspes";
                    }


                    viewRegistres.append(" " + nom + " - " + operacio + " - " + aprovat + "\n");
                } while (c.moveToNext());
            }
            //Boto per tornar a la primera activity
        } else if (v.getId() == R.id.TornaInici) {
            startActivity(i);
        }
    }

}

