package com.example.fidalgo.p3fidalgojavier;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fidalgo on 13/03/2018.
 */

public class Pregunta {
    int idPregunta;
    String enunciat;
    String tema;
    List<Resposta> respostes = new ArrayList<>();

    public Pregunta (int idPregunta, String enunciat, String tema){
        this.idPregunta=idPregunta;
        this.enunciat=enunciat;
        this.tema = tema;
    }

    public Pregunta (int idPregunta, String enunciat, String tema, List<Resposta> respostes){
        this.idPregunta=idPregunta;
        this.enunciat=enunciat;
        this.tema = tema;
        this.respostes = respostes;
    }

    public void afegirResposta(Resposta r){
        respostes.add(r);
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public String getEnunciat() {
        return enunciat;
    }

    public String getTema() {
        return tema;
    }

    public List<Resposta> getRespostes() {
        return respostes;
    }
}
