package com.example.fidalgo.p3fidalgojavier;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Intent i;

    EditText editNom;

    String nombrePers;


    Button btnEliminar;

    RadioButton radioSuma;

    RadioButton radioResta;

    RadioButton radioMulti;

    RadioButton radioDiv;

    private SQLiteDatabase db;

    UsuariosSQLiteHelper usdbh =
            new UsuariosSQLiteHelper(this, "DBUsuarios", null, 1);


    /**
     * Creacio i la insercio de la taula tema
     */
    public void tableTema() {

        //String eliminaTema = "DELETE TABLE Tema";

        String sqlCreateTableTema = "CREATE TABLE Tema (nombre TEXT PRIMARY KEY)";

        String temaSuma = "INSERT INTO tema VALUES('suma');";

        String temaResta = "INSERT INTO tema VALUES('resta');";

        String temaMultiplicacio = "INSERT INTO tema VALUES('multiplicacio');";

        String temaDivisio = "INSERT INTO tema VALUES('divisio');";

        //db.execSQL(eliminaTema);
        db.execSQL(sqlCreateTableTema);
        db.execSQL(temaSuma);
        db.execSQL(temaResta);
        db.execSQL(temaMultiplicacio);
        db.execSQL(temaDivisio);

    }

    /**
     * Creacio i la insercio de la taula pregunta
     */
    public void tablePregunta() {

        String sqlCreateTablePregunta = "CREATE TABLE Pregunta (\n" +
                "     idPregunta INTEGER PRIMARY KEY,\n" +
                "     enunciat TEXT NOT NULL,\n" +
                "     nombre TEXT,\n" +
                "     FOREIGN KEY (nombre) REFERENCES tema(nombre)\n" +
                "     );";

        db.execSQL(sqlCreateTablePregunta);

        String insertSuma1 = "INSERT INTO Pregunta VALUES(1,'Quan es 2 + 2', 'suma');";
        String insertSuma2 = "INSERT INTO Pregunta VALUES(2,'Quan es 100 + 50', 'suma');";
        String insertSuma3 = "INSERT INTO Pregunta VALUES(3,'Quan es 3 + 25', 'suma');";

        String insertResta1 = "INSERT INTO Pregunta VALUES(4,'Quan es 12 - 2', 'resta');";
        String insertResta2 = "INSERT INTO Pregunta VALUES(5,'Quan es 100 - 200', 'resta');";
        String insertResta3 = "INSERT INTO Pregunta VALUES(6,'Quan es 25-5', 'resta');";

        String insertMultiplicacio1 = "INSERT INTO Pregunta VALUES(7,'Quan es 2 * 3', 'multiplicacio');";
        String insertMultiplicacio2 = "INSERT INTO Pregunta VALUES(8,'Quan es 100 * 100', 'multiplicacio');";
        String insertMultiplicacio3 = "INSERT INTO Pregunta VALUES(9,'Quan es 12 * 12', 'multiplicacio');";

        String insertDivisio1 = "INSERT INTO Pregunta VALUES(10,'Quan es 50 / 2', 'divisio');";
        String insertDivisio2 = "INSERT INTO Pregunta VALUES(11,'Quan es 100 / 25', 'divisio');";
        String insertDivisio3 = "INSERT INTO Pregunta VALUES(12,'Quan es 15 / 1', 'divisio');";

        db.execSQL(insertSuma1);
        db.execSQL(insertSuma2);
        db.execSQL(insertSuma3);

        db.execSQL(insertResta1);
        db.execSQL(insertResta2);
        db.execSQL(insertResta3);

        db.execSQL(insertMultiplicacio1);
        db.execSQL(insertMultiplicacio2);
        db.execSQL(insertMultiplicacio3);

        db.execSQL(insertDivisio1);
        db.execSQL(insertDivisio2);
        db.execSQL(insertDivisio3);

    }

    /**
     * Creacio i la insercio de la taula resposta
     */

    public void tableResposta() {



        String sqlCreateTableResposta = "CREATE TABLE Resposta (idResposta INTEGER PRIMARY KEY, idPregunta INTEGER ,resposta TEXT, correcta BOOLEAN,\n" +
                "            FOREIGN KEY(idPregunta) REFERENCES Pregunta(idPregunta))";
         db.execSQL(sqlCreateTableResposta);

        String insert1 = "INSERT INTO Resposta VALUES (1,1,'3','FALSE');";
        db.execSQL(insert1);
        String insert2 = "INSERT INTO Resposta VALUES (2,1,'4', 'TRUE');";
        db.execSQL(insert2);
        String insert3 = "INSERT INTO Resposta VALUES (3,1,'2', 'FALSE');";
        db.execSQL(insert3);

        String insert4 = "INSERT INTO Resposta VALUES (4,2,'150','TRUE');";
        db.execSQL(insert4);
        String insert5 = "INSERT INTO Resposta VALUES (5,2,'4', 'FALSE');";
        db.execSQL(insert5);
        String insert6 = "INSERT INTO Resposta VALUES (6,2,'2', 'FALSE');";
        db.execSQL(insert6);

        String insert7 = "INSERT INTO Resposta VALUES (7,3,'500','FALSE');";
        db.execSQL(insert7);
        String insert8 = "INSERT INTO Resposta VALUES (8,3,'4', 'FALSE');";
        db.execSQL(insert8);
        String insert9 = "INSERT INTO Resposta VALUES (9,3,'28', 'TRUE');";
        db.execSQL(insert9);

        String insert10 = "INSERT INTO Resposta VALUES (10,4,'9','FALSE');";
        db.execSQL(insert10);
        String insert11 = "INSERT INTO Resposta VALUES (11,4,'10', 'TRUE');";
        db.execSQL(insert11);
        String insert12 = "INSERT INTO Resposta VALUES (12,4,'2', 'FALSE');";
        db.execSQL(insert12);

        String insert13 = "INSERT INTO Resposta VALUES (13,5,'-100','TRUE');";
        db.execSQL(insert13);
        String insert14 = "INSERT INTO Resposta VALUES (14,5,'100', 'FALSE');";
        db.execSQL(insert14);
        String insert15 = "INSERT INTO Resposta VALUES (15,5,'0', 'FALSE');";
        db.execSQL(insert15);

        String insert16 = "INSERT INTO Resposta VALUES (16,6,'-100','FALSE');";
        db.execSQL(insert16);

        String insert17 = "INSERT INTO Resposta VALUES (17,6,'130', 'FALSE');";
        db.execSQL(insert17);
        String insert18 = "INSERT INTO Resposta VALUES (18,6,'20', 'TRUE');";
        db.execSQL(insert18);
        String insert19 = "INSERT INTO Resposta VALUES (19,7,'6','TRUE');";
        db.execSQL(insert19);
        String insert20 = "INSERT INTO Resposta VALUES (20,7,'5', 'FALSE');";
        db.execSQL(insert20);
        String insert21 = "INSERT INTO Resposta VALUES (21,7,'4', 'FALSE');";
        db.execSQL(insert21);
        String insert22 = "INSERT INTO Resposta VALUES (22,8,'1.000.000','FALSE');";
        db.execSQL(insert22);
        String insert23 = "INSERT INTO Resposta VALUES (23,8,'10.000', 'TRUE');";
        db.execSQL(insert23);
        String insert24 = "INSERT INTO Resposta VALUES (24,8,'1.000', 'FALSE');";
        db.execSQL(insert24);
        String insert25 = "INSERT INTO Resposta VALUES (25,9,'144','TRUE');";
        db.execSQL(insert25);
        String insert26 = "INSERT INTO Resposta VALUES (26,9,'12.000', 'FALSE');";
        db.execSQL(insert26);
        String insert27 = "INSERT INTO Resposta VALUES (27,9,'1.000.012', 'FALSE');";
        db.execSQL(insert27);
        String insert28 = "INSERT INTO Resposta VALUES (28,10,'25','TRUE');";
        db.execSQL(insert28);
        String insert29 = "INSERT INTO Resposta VALUES (29,10,'5', 'FALSE');";
        db.execSQL(insert29);
        String insert30 = "INSERT INTO Resposta VALUES (30,10,'50', 'FALSE');";
        db.execSQL(insert30);
        String insert31 = "INSERT INTO Resposta VALUES (31,11,'100','FALSE');";
        db.execSQL(insert31);
        String insert32 = "INSERT INTO Resposta VALUES (32,11,'5', 'FALSE');";
        db.execSQL(insert32);
        String insert33 = "INSERT INTO Resposta VALUES (33,11,'4', 'TRUE');";
        db.execSQL(insert33);
        String insert34 = "INSERT INTO Resposta VALUES (34,12,'1','FALSE');";
        db.execSQL(insert34);
        String insert35 = "INSERT INTO Resposta VALUES (35,12,'15', 'TRUE');";
        db.execSQL(insert35);
        String insert36 = "INSERT INTO Resposta VALUES (36,12,'4', 'FALSE');";
        db.execSQL(insert36);


    }

    /**
     * Nomes la creacio de la taula usuaris
     */
    public void tableUsuaris() {
        String sqlCreateTableUsuarios = "CREATE TABLE Usuarios (nombre TEXT, operacio TEXT, superat BOOLEAN)";
        db.execSQL(sqlCreateTableUsuarios);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //------------------------------------------------------------------------------------------

        //Inicializar la conexion
        //UsuariosSQLiteHelper usdbh =
        //new UsuariosSQLiteHelper(this, "DBUsuarios", null, 1);

        db = usdbh.getWritableDatabase();

        //PRIMERA VEGADA DESCOMENTAR EL SEGUENT TEXT PER CREAR E INSERTAR A LES TAULES

       // tableTema();
        //tablePregunta();
        //tableResposta();
       // tableUsuaris();


        //Amb un SELECT en la base de dades guardem els temes
        Cursor c = db.rawQuery("SELECT nombre FROM tema", null);

        String[] temas = new String[4];

        int index = 0;

        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya m�s registros
            do {
                String nom = c.getString(0);
                temas[index] = nom;
                index++;


            } while (c.moveToNext());
        }
        c.close();
        db.close();


        //------------------------------------------------------------------------------------------

        //Inicialitzacio del intent
        i = new Intent(this, PreguntesActivity.class);

        //inicialitzacio del editText on anira el nom
        editNom = (EditText) findViewById(R.id.editTextNom);

        editNom.setOnClickListener(this);

        /* Controlador del boton muerte */
        Button btn = (Button) findViewById(R.id.buttonPreguntes);
        btn.setOnClickListener(this);


        //Controlador que guarda el nom
        Button btnGuarda = (Button) findViewById(R.id.buttonGuardar);
        btnGuarda.setOnClickListener(this);

        //Controlador que carrega el nom guardat
        Button btnCarga = (Button) findViewById(R.id.buttonCarga);

        //-----------------------------------------------------


        //Carga del nom guardat
        btnCarga.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Recuperamos las preferencias
                SharedPreferences prefs =
                        getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);


                nombrePers = prefs.getString("nombre", "nombre_por_defecto");


                editNom.setText(nombrePers);

            }
        });


        //-------------------------------------------------------------


        //Controlador que reseteja el editText
        btnEliminar = (Button) findViewById(R.id.buttonEliminar);
        btnEliminar.setOnClickListener(this);


        //Controlador del primer radioButton
        radioSuma = (RadioButton) findViewById(R.id.radioSuma);
        radioSuma.setText(temas[0]);

        //Controlador del segon radioButton
        radioResta = (RadioButton) findViewById(R.id.radioResta);
        radioResta.setText(temas[1]);

        //Controlador del tercer radioButton
        radioMulti = (RadioButton) findViewById(R.id.radioMulti);
        radioMulti.setText(temas[2]);

        //Controlador del quart radioButton
        radioDiv = (RadioButton) findViewById(R.id.radioDiv);
        radioDiv.setText(temas[3]);


    }

    int numPreguntes;
    String operacio;


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radioSuma:
                if (checked)
                    operacio = "suma";
                break;
            case R.id.radioResta:
                if (checked)
                    operacio = "resta";
                break;
            case R.id.radioMulti:
                if (checked)
                    operacio = "multiplicacio";
                break;
            case R.id.radioDiv:
                if (checked)
                    operacio = "divisio";
                break;
            case R.id.radioPreguntes1:
                if (checked)
                    numPreguntes = 1;
                break;
            case R.id.radioPreguntes2:
                if (checked)
                    numPreguntes = 2;
                break;
            case R.id.radioPreguntes3:
                if (checked)
                    numPreguntes = 3;
                break;
        }
    }

    String nomPersona;

    @Override
    public void onClick(View v) {

        //Creacio del intent al boto que canviara a l'activity de les preguntes
        if (v.getId() == R.id.buttonPreguntes) {

            nomPersona = editNom.getText().toString();

            //Intents
            i.putExtra("operacio", operacio);
            i.putExtra("numeroPreguntes", numPreguntes);
            i.putExtra("nomPersona", nomPersona);

            startActivity(i);

            //Boto per guardar el nom
        } else if (v.getId() == R.id.buttonGuardar) {

            String nomPerson = editNom.getText().toString();

            //Guardamos las preferencias
            SharedPreferences prefs =
                    getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("nombre", nomPerson);
            editor.commit();

            //Boto que reseteja el editText del nom
        } else if (v.getId() == R.id.buttonEliminar) {
            editNom.getText().clear();

            Toast toast2 = Toast.makeText(getApplicationContext(), editNom.getText().toString() + "Nom resetejat", Toast.LENGTH_SHORT);

            toast2.show();


        }
    }
}
