package com.example.fidalgo.p3fidalgojavier;

/**
 * Created by Fidalgo on 13/03/2018.
 */

public class Resposta {


    int idPregunta;
    int idResposta;
    String resposta;
    boolean correcta;

    public Resposta(int idPregunta, int idResposta, String resposta, boolean correcta) {
        this.idPregunta = idPregunta;
        this.idResposta = idResposta;
        this.resposta = resposta;
        this.correcta = correcta;
    }

    public Resposta (){}

    public int getIdResposta() {
        return idResposta;
    }

    public void setIdResposta(int idResposta) {
        this.idResposta = idResposta;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public boolean isCorrecta() {
        return correcta;
    }

    public void setCorrecta(boolean correcta) {
        this.correcta = correcta;
    }

    @Override
    public String toString() {
        return "Resposta{" +
                "idPregunta=" + idPregunta +
                ", idResposta=" + idResposta +
                ", resposta='" + resposta + '\'' +
                ", correcta=" + correcta +
                '}';
    }
}

